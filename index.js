// Конструкция try-catch применяем для локализации возможных ОЖИДАЕМЫХ ошибок,
// прежде всего возможных в следствии отсутствия/некоректного ответа сервера.
// Очень хорошо иллюстрирует данный пример, когда не хватает части данных.
// Либо когда в коде зашиты блоки, которые будут частенько правиться в процессе эксплуатации,
// перезаливаться на сервер итп. Может возникнуть прецедент белого окна, во избежание такого,
// предусматриваем на этот случай реакцию кода, который позволит сделать вывод понятным
// для клиента.

const books = [
  {
    author: "Люсі Фолі",
    name: "Список запрошених",
    price: 70,
  },
  {
    author: "Сюзанна Кларк",
    name: "Джонатан Стрейндж & м-р Норрелл",
  },
  {
    name: "Дизайн. Книга для недизайнерів.",
    price: 70,
  },
  {
    author: "Алан Мур",
    name: "Неономікон",
    price: 70,
  },
  {
    author: "Террі Пратчетт",
    name: "Рухомі картинки",
    price: 40,
  },
  {
    author: "Анґус Гайленд",
    name: "Коти в мистецтві",
  },
];

class NoDataError extends Error {
  constructor(typeOfError) {
    super();
    this.name = "NoDataError";
    this.message = `For this item  of the array "Books", the ${typeOfError} value is undefined`;
  }
};

const container = document.querySelector("#root");
 
class Books {
    constructor(item) {
        if (!item.author) {throw new NoDataError("Author")} else { this.author = item.author };
        if (!item.name) {throw new NoDataError("Name")} else { this.name = item.name };
        if (!item.price) {throw new NoDataError("Price")} else { this.price = item.price };
    }
    render(container) {
        container.insertAdjacentHTML("beforeend",`<p>- THE BOOK -</p><ul><li>Author: ${this.author}</li><li>Name: ${this.name}</li><li>Price: ${this.price}</li></ul>`);
    }
}

books.forEach((el) => {try {new Books(el).render(container)} catch (err) {if (err.name === "NoDataError") {console.warn(err)} else {throw err}}});
